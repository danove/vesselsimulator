#!/usr/bin/python           # This is client.py file

import socket               # Import socket module
import google.protobuf.text_format as tf
import proto.switchboard_pb2 as swb

s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                # Reserve a port for your service.



s.connect((host, port))
while True:
    the_string = s.recv(1024)
    print("the_string", the_string)
s.close()                  # Close the socket when done
