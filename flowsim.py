import logging
from threading import Thread
from time import sleep

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadBuilder, BinaryPayloadDecoder
from pymodbus.server.sync import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import (
    ModbusSlaveContext, ModbusServerContext, ModbusSequentialDataBlock
)


from argparse import ArgumentParser

class Flowsim():
    def __init__(self, slave_ctx):
        self.slave_ctx = slave_ctx


    def get_flow(self):
        values = self.slave_ctx.getValues(3, 0, 48)
        decoder = BinaryPayloadDecoder.fromRegisters(values, byteorder=Endian.Big)
        return decoder.decode_32bit_float()

    
    def set_flow(self, flow):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(flow)
        self.slave_ctx.setValues(3, 0, builder.to_registers())

    def print_state(self):
        print(self.get_flow())

        
def interactive_console(server_context):
    slave_context = server_context[0]
    flowsim = Flowsim(slave_context)
    sleep(0.1)  # Allow main thread to print to stdout first
    the_flow = 10.1
    while True:
        input_text = input("? ('h' for help) ")
        
        if input_text == "h":
            print("h: this help")
            print("p: print state")
            print("t: increment flowrate")
            print("s <hour> <value>: set hour to value")

        if input_text == "p":
            flowsim.print_state()

        if input_text == "t":
            
            flowsim.set_flow(the_flow)
            the_flow += 4.76

def main():
    address = ("localhost", 5020)


    
    slave_context = ModbusSlaveContext(
        r=ModbusSequentialDataBlock(0, 0),
        zero_mode=True,
    )

    server_context = ModbusServerContext(slaves=slave_context, single=True)

    identity = ModbusDeviceIdentification()
    identity.VendorName = "LEVEL Power & Automation"
    identity.ProductName = "LPS test server"
    identity.ProductCode = "LPSTS"

    if args.interactive:
        t = Thread(target=interactive_console,
                   args=(server_context,),
                   daemon=True)
        t.start()

    logging.info("Starting listening on {}:{}".format(address[0], address[1]))
    StartTcpServer(server_context, identity=identity, address=address)

if __name__ == "__main__":
    argparser = ArgumentParser()
    argparser.add_argument("-v", "--verbose", action="store_true",
                           help="Increase verbosity")
    argparser.add_argument("-i", "--interactive", action="store_true",
                           help="Interactive development console")

    args = argparser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    main()


