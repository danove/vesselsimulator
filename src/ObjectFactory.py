import proto.configuration_pb2 as configproto
import google.protobuf.text_format as tf
from src.generator import Generator
from src.thruster import Thruster
from src.breaker import Breaker
from src.bus import Bus
from src.GraphData import GraphData


class ObjectFactory():
    def __init__(self, configuration_file):
        self.configProto = configproto.Configuration()
        configFile = open(configuration_file, 'r')
        tf.Merge(configFile.read(), self.configProto)

    def createGenerators(self):
        generatorlist = []
        for generator in self.configProto.generator:
            generatorlist.append(Generator(generator.index, generator.name, generator.maxPower, GraphData()))
        return generatorlist

    def createPropulsors(self):
        propulsorlist = []
        for propulsor in self.configProto.thruster:
            propulsorlist.append(Thruster(propulsor.name, propulsor.index, propulsor.maxPower))

        return propulsorlist

    def createGeneratorBreakers(self):
        genbreakerlist = []
        for genbrk in self.configProto.generatorbreaker:
            genbreakerlist.append(Breaker(genbrk.index, genbrk.name))

        return genbreakerlist

    def createThrusterBreakers(self):
        thrbrklist = []
        for thrbrk in self.configProto.thrusterbreaker:
            thrbrklist.append(Breaker(thrbrk.index, thrbrk.name))

        return thrbrklist
    def createBuses(self):
        buslist = []
        for bus in self.configProto.bus:
            buslist.append(Bus(bus.index, bus.name))

        return buslist

    def createBusTies(self):
        bustielist = []
        for bustie in self.configProto.bustie:
            bustielist.append(Breaker(bustie.index, bustie.name))

        return bustielist
