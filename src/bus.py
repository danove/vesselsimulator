import sys
from src.GraphNode import GraphNode


class Bus(GraphNode):
    def __init__(self, index, name):
        super().__init__()
        self.__totalLoad = 0.0
        self.__totalReserve = 0.0

        self.__ordered = 0.0
        self.__name = name
        self.__index = index


    def set_order(self, order):
        self.__ordered = order

    def order(self):
        return self.__ordered
    def name(self):
        return self.__name
