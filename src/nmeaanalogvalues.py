#!/usr/bin/python
import sys
from src.nmeacreator import NMEACreator
class NMEAAnalogValues(NMEACreator):

    def __init__(self, telegramindex, numvalues):

        self.__type = 'M'
        self.__telegramindex = telegramindex
        
        self.__analogvalues = [0.0] * numvalues
        NMEACreator.__init__(self, self.__type, self.__telegramindex)

    def createString(self):
        NMEACreator.createString(self)
        nmeastring = NMEACreator.nmeastring(self)
       
        for value in self.__analogvalues:
            nmeastring = nmeastring + ',' + str(value)

        self.set_nmeastring(nmeastring)
        self.calcChecksum()
        self.appendCRC()
        self.appendCRLF()

    def set_values(self, values):
        self.__analogvalues = values
        



    
