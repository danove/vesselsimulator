from numpy import interp
import random


class VesselModel():
    def __init__(self):
        self.maxPower = 7000.0

        self.power_table = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        self.velocity_table = [0, 2.5, 4, 5, 6, 8, 10, 12, 13, 14, 14.5]
        self.fuel_table = [0, 25, 50, 100, 150, 200, 250, 300, 325, 350, 400]
    
    def velocity_from_mpr_power(self, power):
        vel = 0.0
        vel = interp((power/self.maxPower)*100,
                     self.power_table, self.velocity_table)

        return vel

    def velocity_from_mpr_power_with_noise(self, power):
        vel = self.velocity_from_mpr_power(power)
        randrange = vel * 0.1
        vel = vel + random.uniform(-randrange, randrange)
        return vel

    def fuel_usage_from_mpr_power(self, power):
        fuel_liters_hour = 0.0
        fuel_liters_hour = interp((power/self.maxPower)*100,
                                  self.power_table, self.fuel_table)
        return fuel_liters_hour

