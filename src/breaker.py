import sys
from src.GraphNode import GraphNode

class Breaker(GraphNode):
    def __init__(self, index, name):
        super().__init__()
        self.__state = False
        self.__index = index
        self.__name = name
    def set_breaker_state(self, state):
        self.__state = state

    def state(self):
        return self.__state

    def name(self):
        return self.__name