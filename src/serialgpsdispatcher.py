from collections import deque


class SerialGpsDispatcher:
    def __init__(self, serialPort):
        self.serialport = serialPort
        self.sentenceQueue = deque()
        
    def addNMEASentence(self, sentence):
        self.sentenceQueue.append(sentence)

    def sendMessage(self):
        if len(self.sentenceQueue) > 0:
            message = self.sentenceQueue.popleft()
            print(message)
            self.serialport.write(message)
