import sys
import random
import datetime

class GpsData():
    def __init__(self):
        # lat/lon in degrees decimals
        self.latitude = 59.01211
        self.longitude = 5.298734
        self.latSign = 'N'
        self.longSign = 'E'
        self.heading = 145.2
        self.SOG = 0.0
        self._utc_time = '11.11.09'
        self._utc_date = '08.06.2018'
        self.failure_ind = 0
        self.fail_max_idx = 20

    @property
    def utc_time(self):
        timestamp = datetime.datetime.utcnow()
        self._utc_time = str(timestamp.hour) + ':' + str(timestamp.minute) + ':' + str(timestamp.second)
        return self._utc_time

    @utc_time.setter
    def utc_time(self, time):
        timestamp = datetime.datetime.utcnow()
        self._utc_time = str(timestamp.hour) + ':' + str(timestamp.minute) + ':' + str(timestamp.second)
    @property
    def utc_date(self):
        timestamp = datetime.datetime.utcnow()
        self._utc_date = str(timestamp.year) + ':' + str(timestamp.month) + ':' + str(timestamp.day)
        return self._utc_date

    @utc_date.setter
    def utc_date(self, time):
        timestamp = datetime.datetime.utcnow()
        self.utc_date = str(timestamp.year) + ':' + str(timestamp.month) + ':' + str(timestamp.day)

    def setLatitude(self, latpos):
        self.latitude = latpos

    def setLongitude(self, lonpos):
        self.longitude = lonpos

    def setVesselHeading(self, heading):
        self.heading = heading

    def setSpeedOverGround(self, velocity):
        self.SOG = velocity

    def lat(self):
        return self.latitude

    def lon(self):
        return self.longitude

    def time_step(self):

        if self.failure_ind < self.fail_max_idx:
            self.latitude += 0.0001
            self.longitude += 0.0001
#            self.SOG = random.randint(3, 9)
            self.heading = random.randint(0, 359)
            self.failure_ind += 1
        else:
            self.latitude += 0.0001
            self.longitude += 0.0001
 #           self.SOG = '##'
            self.heading = '#'
            self._utctime = '#'
            self._utcdate = '#'
            
            self.failure_ind = 0


        
