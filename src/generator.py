import sys
from src.GraphNode import GraphNode
from PyQt5.QtCore import QObject, pyqtSlot, pyqtSignal,QPoint


from numpy import interp

class Generator(GraphNode):
    generatorfeedbackupdated = pyqtSignal(int, arguments=['generator_fb'])
    fuelconsumptionupdated = pyqtSignal(float, arguments=['fuel_fb'])
    def __init__(self, index, name, capacity, graphdata):
        super().__init__()
        self.__size_in_kw = capacity
        self.__order = 0.0
        self.__feedback = 0.0
        self.__ramp_factor = 10
        self.__name = name
        self.__index = index
        self.__graphdata = graphdata
        self.fuelX = [0, 10, 20, 25, 30, 40, 50, 60, 70, 75, 80, 90, 100]
        self.fuelRate = [0.0, 317.2, 251.0, 238.4, 230.9, 221.6, 215.9, 210.9,
                         206.9, 205.1, 203.3, 200.3, 197.9]

        self._fuel_rate = 0.0
        self.flow_to_engine = 0
        self.flow_from_engine = 0
        self.ureaflow = 0
    @property
    def fuel_rate(self):
        return self._fuel_rate
    
    @pyqtSlot(int)
    def set_order(self, order):
        self.__order = order

    def name(self):
        return self.__name
    def order(self):
        return self.__order
    def feedback(self):
        return self.__feedback

    def name(self):
        return self.__name

    def time_step(self):

        """
        runs at 10Hz
        :return:
        """

        if self.__feedback < self.__order:
            self.__feedback = self.__feedback + self.__ramp_factor
            self.generatorfeedbackupdated.emit(self.__feedback)
            self._fuel_rate = interp((self.__feedback/self.__size_in_kw)*100, self.fuelX, self.fuelRate)
            
            self.fuelconsumptionupdated.emit(self._fuel_rate)
            self.__graphdata.append(self._fuel_rate)
        elif self.__feedback > self.__order:
            self.__feedback = self.__feedback - self.__ramp_factor
            self.generatorfeedbackupdated.emit(self.__feedback)
            self._fuel_rate = interp((self.__feedback / self.__size_in_kw) * 100, self.fuelX, self.fuelRate)
            self.fuelconsumptionupdated.emit(self._fuel_rate)
            self.__graphdata.append(self._fuel_rate)
        elif self.__feedback == self.__order:
            self.generatorfeedbackupdated.emit(self.__feedback)
            self._fuel_rate = interp((self.__feedback / self.__size_in_kw) * 100, self.fuelX, self.fuelRate)
            self.fuelconsumptionupdated.emit(self._fuel_rate)
            self.__graphdata.append(self._fuel_rate)

    # @pyqtSlot(QXYSeries)
    # def update(self, xySeries):
    #     print("update")

    def graph_data(self):
        return self.__graphdata

    def setDieselFlowToEngine(self, flow):
        self.flow_to_engine = flow

    def setDieselFlowFromEngine(self, flow):
        self.flow_from_engine = flow

    def ureaflow(self, flow):
        self.ureaflow = flow
        
    
        
