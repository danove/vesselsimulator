#!/usr/bin/python

import socket
import google.protobuf.text_format


class NetworkServer():
    def __init__(self):
        self.socket= socket.socket()
        host = socket.gethostname()
        port = 12345
        self.socket.bind((host, port))
        self.socket.listen(5)


    def send(self, thebytes):
        c, addr = self.socket.accept()
        print ('Got connection from', addr)
        c.send(thebytes)
        print("sending...", thebytes)
        c.close()


