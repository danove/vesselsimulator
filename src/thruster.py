import sys
from src.GraphNode import GraphNode


class Thruster(GraphNode):
#    thrusterfeedbackupdated = pyqtSignal(int, arguments=['thruster_fb'])

    def __init__(self, name, sindex, size):
        super(Thruster, self).__init__()
        self.__size_in_kw = size
        self.__order_in_kw = 0.0
        self.__feedback_in_kw = 0.0
        self.__rampfactor = 10.0
        self.__name = name
        self.__index = sindex

    def feedback(self):
        return self.__feedback_in_kw

    def set_kw_order(self, order):
        self.__order_in_kw = order

    def order(self):
        return self.__order_in_kw

    def setOrderInPct(self, order):
        self.set_kw_order((order/100) * self.__size_in_kw)

    def name(self):
        return self.__name

    def time_step(self):
        if self.__feedback_in_kw < self.__order_in_kw:
            self.__feedback_in_kw +=self.__rampfactor
        elif self.__feedback_in_kw > self.__order_in_kw:
            self.__feedback_in_kw -= self.__rampfactor
        elif self.__feedback_in_kw == self.__order_in_kw:
            pass


