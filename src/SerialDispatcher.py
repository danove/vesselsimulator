#!/usr/bin/python
import serial
from src.basedispatcher import BaseDispatcher

class SerialDispatcher(BaseDispatcher):
    def __init__(self, serialPort):
        self.serialPort = serialPort

    def send(self, the_string):
#        print("SerialDispatcher.send() ", the_string)
        self.serialPort.write(the_string)
#        self.serialPort.write(the_string.encode('utf-8'))


    def disconnect(self):
        self.serialPort.close()





            
