import random
import sys
import google.protobuf.text_format as tf
import app.proto.switchboard_pb2 as switchboardproto
from PyQt5.QtCore import pyqtSlot, QObject
import src.modbusclient as modbusclient
from numpy import interp
import logging
import src.analogoutputs as analogoutputs


class Switchboard(QObject):
    def __init__(self, object_factory,
                 graph,
                 nmeagps,
                 config_proto,
                 modbusdata,
                 gpsdata,
                 gpsdispatcher,
                 iasdata):
        super(Switchboard, self).__init__()
        self.logger = logging.getLogger("simulator")
        self.__graph = graph
        self.nmeagps = nmeagps
        self.configurationProto = config_proto
        self.objectfactory = object_factory
        self.generators = []
        self.propulsors = []
        self.thrusterlist = []
        self.generatorlist = []

        self.generators = object_factory.createGenerators()
        self.propulsors = object_factory.createPropulsors()
        self.generatorbreakers = object_factory.createGeneratorBreakers()
        self.thrusterbreakers = object_factory.createThrusterBreakers()
        self.buses = object_factory.createBuses()
        self.busties = object_factory.createBusTies()
        self.__graph.setGenerators(self.generators)
        self.__graph.setGeneratorBreakers(self.generatorbreakers)
        self.__graph.setThrusterBreakers(self.thrusterbreakers)
        self.__graph.setPropulsors(self.propulsors)
        self.__graph.setBuses(self.buses)
        self.__graph.setBusTies(self.busties)
        self.modbusdata = modbusdata
        self.gpsdata = gpsdata
        self.gpsserialdispatcher = gpsdispatcher
        self.iasdata = iasdata
        #        self.__graph.createAdjacenciesList()
        # for i in range(0, len(self.generators)):
        #     self.switchboard_proto.generator.add()
        #     self.switchboard_proto.generator[i].name = self.generators[i].name()
        #     print(self.generators[i].name())
        # for i in range(0, len(self.propulsors)):
        #     self.switchboard_proto.thruster.add()
        #     self.switchboard_proto.thruster[i].name = self.propulsors[i].name()
        self.sog = [0.0, 1.0, 2.0, 3.0, 4.0,
                    5.0, 6.0, 7.0, 8.0, 9.0,
                    10.0, 11.0, 12.0, 13.0]
        self.mainenginefuelflow = [0, 35.0, 70.0, 105.0, 140.0,
                                   175.0, 210.0, 245.0, 280.0, 315.0,
                                   350.0, 385.0, 420.0, 455.0]

    def reset_tree_nodes(self):
        for item in self.__graph.adj_list:
            item.setVisited(False)
        self.thrusterlist = []
        self.generatorlist = []

    def generators(self):
        return self.generators

    def setGenBreakerStatus(self, genBreakerIndex, status):
        if genBreakerIndex == 1:
            if status is False:
                self.__graph.adj_list.pop(self.genbreaker1)
            else:
                self.__graph.adj_list[self.genbreaker1] = [self.generator1, self.portbus]
        elif genBreakerIndex == 2:
            if status is False:
                self.__graph.adj_list.pop(self.genbreaker2)
            else:
                self.__graph[self.genbreaker2] = [self.generator2, self.portbus]
        elif genBreakerIndex == 3:
            if status is False:
                self.__graph.adj_list.pop(self.genbreaker3)
            else:
                self.__graph[self.genbreaker3] = [self.generator3, self.stbdbus]
        elif genBreakerIndex == 4:
            if status is False:
                self.__graph.adj_list.pop(self.genbreaker4)
            else:
                self.__graph[self.genbreaker4] = [self.generator4, self.stbdbus]
        else:
            pass

    def setThrusterBreakerStatus(self, thrBreakerIndex, status):
        if thrBreakerIndex == 1:
            if status is False:
                self.__graph.adj_list.pop(self.thrusterbreaker1)
            else:
                self.__graph.adj_list[self.thrusterbreaker1] = [self.thruster1, self.portbus]

        elif thrBreakerIndex == 2:
            if status is False:
                self.__graph.adj_list.pop(self.thrusterbreaker2)
            else:
                self.__graph.adj_list[self.thrusterbreaker2] = [self.thruster2, self.stbdbus]
        elif thrBreakerIndex == 3:
            if status is False:
                self.__graph.adj_list.pop(self.thrusterbreaker3)
            else:
                self.__graph.adj_list[self.thrusterbreaker3] = [self.thruster3, self.portbus]
        elif thrBreakerIndex == 4:
            if status == False:
                self.__graph.adj_list.pop(self.thrusterbreaker4)
            else:
                self.__graph.adj_list[self.thrusterbreaker4] = [self.thruster4, self.stbdbus]

        elif thrBreakerIndex == 5:
            if status == False:
                self.__graph.adj_list.pop(self.thrusterbreaker5)
            else:
                self.__graph.adj_list[self.thrusterbreaker5] = [self.thruster5, self.portbus]
        else:
            pass

    def setBusTieStatus(self, status):
        if status == False:
            del self.__graph.adj_list[self.bustie]
        else:
            self.__graph.adj_list[self.bustie] = [self.portbus, self.stbdbus]

    @pyqtSlot(int, int)
    def setThrustOrderInPct(self, index, order):
        self.propulsors[index-1].setOrderInPct(order)

    @pyqtSlot(int, int)
    def setThrustOrderInKW(self, index, order):
        print("setting %i on thruster %i", index, order)
        self.propulsors[index-1].set_kw_order(order)

    def updateBusOrder(self):
        number_of_buses = len(self.buses)
        if number_of_buses == 2:
            if self.busties[0] in self.__graph.adj_list:
                pass
                #print(self.busties[0])
        if self.busties[0] in self.__graph.adj_list:
            #closed bus, consider as one bus
            #print ("closed bustie")
            total_order = 0.0
            thruster_list = self.__graph.findThrustersConnectedToNode(self.busties[0])
            self.reset_tree_nodes()
            for thruster in thruster_list:
                total_order += thruster.order()
            self.buses[0].set_order(total_order)
        else:
            port_order = 0.0
            stbd_order = 0.0
            self.reset_tree_nodes()
            port_thrusters = self.__graph.findThrustersConnectedToNode(self.buses[0])

            self.reset_tree_nodes()
            stbd_thrusters = self.__graph.findThrustersConnectedToNode(self.buses[1])
            self.reset_tree_nodes()
            for items in port_thrusters:
                port_order+= items.order()
            self.buses[0].set_order(port_order)
            for thruster in stbd_thrusters:
                stbd_order+= thruster.order()
            self.buses[1].set_order(stbd_order)

    @pyqtSlot(int, int)
    def setGeneratorLoad(self, genIndex, load):
        print("setGeneratorLoad: ", genIndex, ":", load)
        self.generators[genIndex].set_order(load)
        
    def updateGeneratorOrder(self):
        if self.busties[0] in self.__graph.adj_list:
            gen_list = self.__graph.findGeneratorsConnectedToNode(self.busties[0])
            self.reset_tree_nodes()
            num_gens_online = len(gen_list)
          #  print("bus 1 order: ", self.buses[0].order())
          #  print("bus 2 order: ", self.buses[1].order())
            for generator in gen_list:
                generator.set_order(self.buses[0].order()/num_gens_online)

        else: # open bustie
            port_gen_list = self.__graph.findGeneratorsConnectedToNode(self.buses[0])
            self.reset_tree_nodes()
            stbd_gen_list = self.__graph.findGeneratorsConnectedToNode(self.buses[1])
            self.reset_tree_nodes()
            number_of_port_gens = len(port_gen_list)
            number_of_stbd_gens = len(stbd_gen_list)

            for generator in port_gen_list:
                generator.set_order(self.buses[0].order()/number_of_port_gens)


            for generator in stbd_gen_list:
                generator.set_order(self.buses[1].order()/number_of_stbd_gens)

    def time_step(self):

        for generator in self.generators:
            generator.time_step()

        for thruster in self.propulsors:
            thruster.time_step()
        self.gpsdata.time_step()

        print("flow to engine: ", self.generators[0].flow_to_engine)
        sog = interp(self.generators[0].flow_to_engine, self.mainenginefuelflow, self.sog)
        print("sog: ", sog)
        self.gpsdata.setSpeedOverGround(sog)

        self.nmeagps.createGGA(self.gpsdata)
        self.nmeagps.createVTG(self.gpsdata)
        self.nmeagps.createHDT(self.gpsdata)
        self.nmeagps.createGNS(self.gpsdata)
        self.nmeagps.createRMB(self.gpsdata)
        self.nmeagps.createRMC(self.gpsdata)
        if self.gpsserialdispatcher is not None:
            self.gpsserialdispatcher.addNMEASentence(self.nmeagps.gga_string)
            self.gpsserialdispatcher.addNMEASentence(self.nmeagps.vtg_string)
            self.gpsserialdispatcher.addNMEASentence(self.nmeagps.gns_string)
            self.gpsserialdispatcher.addNMEASentence(self.nmeagps.rmb_string)
            self.gpsserialdispatcher.sendMessage()
            self.gpsserialdispatcher.sendMessage()
            self.gpsserialdispatcher.sendMessage()
            self.gpsserialdispatcher.sendMessage()
        
        thrusters = self.configurationProto.thruster
        generators = self.configurationProto.generator

        if self.configurationProto.HasField("modbusconnection"):

            for idx, thruster in enumerate(thrusters):
                if thruster.load.feedback.HasField("modbus"):
                    self.modbusdata.setThrusterLoad(idx, self.propulsors[idx].order()) 

            for idx, generator in enumerate(generators):
                if generator.load.feedback.HasField("modbus"):
                    self.modbusdata.setGeneratorLoad(idx, self.generators[idx].feedback())
                    analogoutputs.setAnalogOutput(idx, self.generators[idx].feedback()/self.generators.maxPower())
                for index, flow in enumerate(generator.flow):
                    if flow.flowtype == 0:
                        if flow.index == 1:
                            self.modbusdata.setDieselFlowToEngine(idx, self.generators[idx].flow_to_engine)
                        if flow.index == 2:
                            self.modbusdata.setDieselFlowFromEngine(idx, self.generators[idx].flow_from_engine)                       
                    if flow.flowtype == 1:  # ureaFlow
                        self.modbusdata.setUreaFlow(idx, self.generators[idx].ureaflow)

        if self.configurationProto.HasField("iasconnection"):
            for idx, thruster in enumerate(thrusters):
                if thruster.load.feedback.HasField("nmea"):
                    print("setting thruster load on iasconnection", idx, ":", self.propulsors[idx].order())
                    self.iasdata.setThrusterLoad(idx, self.propulsors[idx].order())

            for idx, generator in enumerate(generators):
                if generator.load.feedback.HasField("nmea"):
                    print("setting generator load on iasconnection")
                    self.iasdata.setGeneratorLoad(idx, self.generators[idx].feedback())
#            self.iasdata.transmitdata()
        # self.updateGeneratorOrder()
        #populate proto
        i = 0
        # for gen in self.switchboard_proto.generator:
        #     gen.load_actual = self.generators[i]p.feedback()
        #     i = i + 1

        # i = 0
        # for thr in self.switchboard_proto.thruster:
        #     thr.load_actual = self.propulsors[i].feedback()
        #     print(i, "           ", thr.load_actual)
        #     i = i + 1
        # hostname = self.configurationProto.iasinterface.hostname
        # portnumber = self.configurationProto.iasinterface.portnumber
        
        # modbusinterface = self.configurationProto.iasinterface.modbusinterface
        # for reg in modbusinterface.genfuellph:
        #     print("reg address: ", reg.startAddress)
        #     fb  = modbusclient.read(hostname, portnumber, reg.startAddress, reg.numberOfRegisters)
        #     print("lph fb: ", fb)

        # for reg in modbusinterface.genfuelrate:
        #     print("reg address: ", reg.startAddress)
        #     fb  = modbusclient.read(hostname, portnumber, reg.startAddress, reg.numberOfRegisters)
        #     print("fuel rate fb: ", fb)

        # totalrunningfuelusage_reg = modbusinterface.totalrunningfuelusage[0]
        # totalrunningfuelusage = modbusclient.read(hostname, portnumber, totalrunningfuelusage_reg.startAddress, totalrunningfuelusage_reg.numberOfRegisters)
        # print("Total running fuel usage: ", totalrunningfuelusage)

    @pyqtSlot(result=int)
    def get_number_of_generators(self):
        return len(self.generators)

    @pyqtSlot(result=int)
    def get_number_of_propulsors(self):
        return len(self.propulsors)

    def generator(self, index):
        return self.generators[index-1]

    def propulsor(self, index):
        return self.propulsors[index]

    def load(self, index):
        return self.generators[index-1].feedback()

    def parseconfiguration(self):
        #  create switchboard components
        #  setup graph with adjacency list
        pass

    @pyqtSlot(int, result=str)
    def generatorName(self, generatorindex):
        return self.generators[generatorindex].name()

    @pyqtSlot(int, result=str)
    def propulsorName(self, index):
        return self.propulsor(index-1).name()

    @pyqtSlot(int, int)
    def setDieselFlowToEngine(self, genIndex, flow):
        self.generators[genIndex].setDieselFlowToEngine(flow)

    @pyqtSlot(int, int)
    def setDieselFlowFromEngine(self, genIndex, flow):
        self.generators[genIndex].setDieselFlowFromEngine(flow)

    @pyqtSlot(int, int)
    def setUreaFlow(self, genIndex, flow):
        self.generators[genIndex].ureaflow = flow
