#!/usr/bin/python
import socket
import sys
import logging
from src.basedispatcher import BaseDispatcher
logger = logging.getLogger("vesselsimulator")


class MessageDispatcher(BaseDispatcher):
    def __init__(self, target_host, target_port):
        super().__init__(target_host)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        logger.info("Sending UDP packets to {}:{}".format(target_host,
                                                          target_port))
        self.clientaddress = (target_host, target_port)

    def send(self, the_string):
        print("sending UDP to :", self.clientaddress)
        try:
            self.socket.sendto(the_string.encode('ascii'), self.clientaddress)
        except BrokenPipeError:
            print("BrokenPipeError")

    def disconnect(self):
        self.socket.close()
