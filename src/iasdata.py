

class IASData:
    def __init__(self, configuration):
        self.config = configuration
        self.thrusterloads = [0.0] * 5
        self.generatorloads = [0.0] * 4

    def setThrusterLoad(self, idx, load):
        # print("IASData.setThrusterLoad()")
        # print("idx: ", idx)
        # print("load: ", load)
        self.thrusterloads[idx-1] = load

    def setGeneratorLoad(self, idx, load):
        # print("IASData.setGeneratorLoad()")
        # print("idx: ", idx)
        # print("load: ", load)

        self.generatorloads[idx-1] = load


        
