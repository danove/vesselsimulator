#!/usr/bin/python
import sys

class NMEACreator():
    def __init__(self, type, telegramindex=0):
        self.__nmeastring = ''
        self.__telegramandvendor = '$PWNO'
        self.__type = type
        self.__chksum = 0
        self.__telegramindex = telegramindex

    def createString(self):
        self.__nmeastring = self.__telegramandvendor + self.__type
        
        if (self.__telegramindex):
            self.__nmeastring = self.__nmeastring + str(self.__telegramindex)

    def calcChecksum(self):
        i = 1
        while i < len(self.__nmeastring):
            char = ord(self.__nmeastring[i])
            self.__chksum = self.__chksum ^ char
            i = i + 1

    def set_telegram_type(self, type):
        self.__type = type
    
    def nmeastring(self):
        return self.__nmeastring

    def set_nmeastring(self, the_string):
        self.__nmeastring = the_string

    def appendCRC(self):
        self.__nmeastring = self.__nmeastring + '*' + hex(self.__chksum)[2:]

    def appendCRLF(self):
        self.__nmeastring = self.__nmeastring + '\r\n'
