import random


class Scenario():
    def __init__(self, switchboard):
        self.cycle_number = 1
        self.switchboard = switchboard

    def time_step(self):
        number_of_cycles = 6
        
        thruster1_order = 15 * self.cycle_number + random.randint(-5, 5)
        thruster2_order = 15 * self.cycle_number + random.randint(-5, 5)
        thruster3_order = 15 * self.cycle_number + random.randint(-5, 5)
        thruster4_order = 15 * self.cycle_number + random.randint(-5, 5)
        thruster5_order = 15 * self.cycle_number + random.randint(-5, 5)
        self.switchboard.setThrustOrderInPct(1, thruster1_order)
        self.switchboard.setThrustOrderInPct(2, thruster2_order)
        self.switchboard.setThrustOrderInPct(3, thruster3_order)
        self.switchboard.setThrustOrderInPct(4, thruster4_order)
        self.switchboard.setThrustOrderInPct(5, thruster5_order)
        self.cycle_number += 1
        if self.cycle_number > number_of_cycles:
            self.cycle_number = 1
