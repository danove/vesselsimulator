from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadBuilder
from scipy import interpolate
# import random


class ModbusData():
    def __init__(self, slave_context, config_proto):
        self.slave_ctx = slave_context
        self.configproto = config_proto

    def set_flow(self, flow_data):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_16bit_int(int(flow_data))
        self.slave_ctx.setValues(3, 4,
                                 builder.to_registers())

    def setGeneratorLoad(self, generator_index, load):
        rawval = load
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(rawval)
        generator = self.configproto.generator[generator_index]
        func_code = 3
        start_address = generator.load.feedback.modbus.startAddress

        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setThrusterLoad(self, thruster_index, load):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(load)
        func_code = 3
        thruster = self.configproto.thruster[thruster_index]
        address = thruster.load.feedback.modbus.startAddress
        self.slave_ctx.setValues(func_code, address,
                                 builder.to_registers())

    def setWaveHs(self, wavehs):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(wavehs)

        modbusinterface = self.configproto.iasinterface.modbusinterface
        start_address = modbusinterface.wavehs[0].startAddress
        func_code = modbusinterface.wavehs[0].functionCode
        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setWaveDir(self, wavedir):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(wavedir)
#        print("setting wavedir............", wavedir)
        modbusinterface = self.configproto.iasinterface.modbusinterface
        start_address = modbusinterface.wavedir[0].startAddress
        func_code = modbusinterface.wavedir[0].functionCode
        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setWindSpeed(self, windspeed):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(windspeed)
#        print("setting windspeed............", windspeed)
        modbusinterface = self.configproto.iasinterface.modbusinterface
        start_address = modbusinterface.windspeed[0].startAddress
        func_code = modbusinterface.windspeed[0].functionCode
        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setWindDir(self, winddir):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(winddir)
#        print("setting winddir............", winddir)
        modbusinterface = self.configproto.iasinterface.modbusinterface
        start_address = modbusinterface.winddir[0].startAddress
        func_code = modbusinterface.winddir[0].functionCode
        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setCurrentSpeed(self, currentspeed):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(currentspeed)
#        print("setting currentspeed............", currentspeed)
        modbusinterface = self.configproto.iasinterface.modbusinterface
        start_address = modbusinterface.currentspeed[0].startAddress
        func_code = modbusinterface.currentspeed[0].functionCode
        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setCurrentDir(self, currentdir):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(currentdir)
#        print("setting currentdir............", currentdir)
        modbusinterface = self.configproto.iasinterface.modbusinterface
        start_address = modbusinterface.currentdir[0].startAddress
        func_code = modbusinterface.currentdir[0].functionCode
        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setMode(self, mode):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_8bit_int(mode)
#        print("setting mode............", mode)
        modbusinterface = self.configproto.iasinterface.modbusinterface
        start_address = modbusinterface.mode[0].startAddress
        func_code = modbusinterface.mode[0].functionCode
        self.slave_ctx.setValues(func_code, start_address,
                                 builder.to_registers())

    def setDieselFlowToEngine(self, generatorindex, value):
        print("setDieselFlowToEngine: ", generatorindex, " ", value)
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(value)
        generator = self.configproto.generator[generatorindex]
        address = generator.flow[0].feedback.modbus.startAddress

        func_code = 3
        self.slave_ctx.setValues(func_code, address,
                                 builder.to_registers())

    def setDieselFlowFromEngine(self, generatorindex, value):
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        builder.add_32bit_float(value)
        func_code = 3
        generator = self.configproto.generator[generatorindex]
        address = generator.flow[1].feedback.modbus.startAddress

        self.slave_ctx.setValues(func_code, address,
                                 builder.to_registers())

    def setUreaFlow(self, generator_index, value):

        generator = self.configproto.generator[generator_index]
        for flow in generator.flow:
            if flow.flowtype == 1:
                address = flow.feedback.modbus.startAddress
                builder = BinaryPayloadBuilder(byteorder=Endian.Big)
                builder.add_32bit_float(value)
                func_code = 3
                self.slave_ctx.setValues(func_code, address,
                                         builder.to_registers())
            else:
                pass
