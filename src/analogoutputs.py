import widgetlords
import utilsclass


widgetlords.pi_spi.init()
outputs = widgetlords.pi_spi.Mod2AO()
zero_ma = 745
twenty_ma = 3723


def setAnalogOutput(channel, percentOfMax):
    if (channel >= 0 and channel <= 4):
        outputs.write_single(channel,
                             utilsclass.convert_percent_to_rawvalue(percentOfMax))
