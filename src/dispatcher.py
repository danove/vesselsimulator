#!/usr/bin/python
import socket
import sys
import app.proto.switchboard_pb2 as swb
import src.nmeaanalogvalues as nmeaanalog
import random
import src.nmeaswbdstates as nmeadigital
import src.nmeagps as nmeagps
import src.vesselmodel as vessel
import logging

logger = logging.getLogger("vesselsimulator")


class Dispatcher():
    def __init__(self, msg_dispatcher,
                 modbusdata, generators, gps_data,
                 seriaDispatcher, iasdata):

        self.message_dispatcher = msg_dispatcher
        self.gps_data = gps_data
        self.vesselmodel = vessel.VesselModel()
        self.sent_packets = set()
        self.modbusdata = modbusdata
        self.serialDispatcher = seriaDispatcher
        self.mpr_power = 0.0
        self.nmea_digital = nmeadigital.NMEASwbdStates()
        self.ias_data = iasdata
        # thruster loads
        self.nmea_analog_1 = nmeaanalog.NMEAAnalogValues(1, 5)
        # generator loads
        self.nmea_analog_2 = nmeaanalog.NMEAAnalogValues(2, 4)
        # draft readings
        self.nmea_analog_3 = nmeaanalog.NMEAAnalogValues(3, 6)
        # gps data
        self.nmea_analog_4 = nmeaanalog.NMEAAnalogValues(4, 7)
        self.nmeagps = nmeagps.NMEAGps()
        self.generators = generators
        
    def time_step(self):
        breaker_values = [0] * 21
        thrusterload_values = [0.0] * 5
        genload_values = [0.0] * 4

        thrusterload_values[0] = self.ias_data.thrusterloads[0]
        for i in range(5):
            thrusterload_values[i] = self.ias_data.thrusterloads[i]

        for i in range(4):
            genload_values[i] = self.ias_data.generatorloads[i]
            
        # thrusterload_values[0] = self.switchboardproto.thruster[2].load_actual #bow azi
        # thrusterload_values[1] = self.switchboardproto.thruster[0].load_actual # tunnel1
        # thrusterload_values[2] = self.switchboardproto.thruster[1].load_actual # tunnel2
        # thrusterload_values[3] = self.switchboardproto.thruster[3].load_actual # port azi
        # thrusterload_values[4] = self.switchboardproto.thruster[4].load_actual # stbd azi
        self.mpr_power = thrusterload_values[3] + thrusterload_values[4]

#        self.modbusdata.set_flow(12)
        j = 0
#        self.modbusdata.setThrusterLoad(0, 23)
        # for thrload in thrusterload_values:
            
        #    self.modbusdata.setThrusterLoad(j, thrload)
        #     j = j + 1

        # i = 0
        # offset = 1
#         for gen in self.switchboardproto.generator:
# #           genload_values[i] = gen.loadInKW
#             genload_values[i] = gen.load_actual
#             self.modbusdata.setDieselFlow(i, 40000 + offset, gen.load_actual) 
#             print("gen: ", i)
 #             print("diesel flowaddress: ", 40000 + offset)
#             print("flow: ", gen.load_actual)
#             self.modbusdata.setGeneratorLoad(i, gen.load_actual)
#             self.modbusdata.setUreaFlow(i, 40008 + offset, gen.load_actual/4)
#             print("urea flowaddress: ", 40008 + offset)
#             print("flow: ", gen.load_actual/4)
#             i = i + 1
#             offset += 2
            
        # waveDir = round(random.uniform(0.0, 359.9))
        # windDir = round(random.uniform(0.0, 359.9))
        # currentDir = round(random.uniform(0.0, 359.9))
        # waveHs = round(random.uniform(0.0, 9.9))
        # currentSpeed = round(random.uniform(0.0, 2.0))
        # windSpeed = round(random.uniform(0.0, 60.0))
        
        # self.modbusdata.setWaveDir(waveDir)
        # self.modbusdata.setWaveHs(waveHs)
        # self.modbusdata.setWindDir(windDir)
        # self.modbusdata.setWindSpeed(windSpeed)
        # self.modbusdata.setCurrentDir(currentDir)
        # self.modbusdata.setCurrentSpeed(currentSpeed)
        # self.modbusdata.setMode(1)
        velocity = self.vesselmodel.velocity_from_mpr_power_with_noise(self.mpr_power)
        # liters_hour = self.vesselmodel.fuel_usage_from_mpr_power(self.mpr_power)
        # velocity = round(velocity, 2)
        # self.modbusdata.set_flow(liters_hour+100)
#        self.gps_data.setSpeedOverGround(velocity)
        # self.nmeagps.createGNS(self.gps_data)
        # self.nmeagps.createVTG(self.gps_data)
        # self.nmeagps.createGGA(self.gps_data)
        # self.nmeagps.createRMB(self.gps_data)
        # self.nmeagps.createRMC(self.gps_data)
        # self.nmeagps.createZDA(self.gps_data)

        # self.message_dispatcher.send(self.nmeagps.gga_string)
        # self.message_dispatcher.send(self.nmeagps.zda_string)
        # self.message_dispatcher.send(self.nmeagps.rmb_string)
        # self.message_dispatcher.send(self.nmeagps.rmc_string)
        # self.message_dispatcher.send(self.nmeagps.vtg_string)
        # if self.serialDispatcher:
        #     self.serialDispatcher.send(self.nmeagps.vtg_string)
        #     self.serialDispatcher.send(self.nmeagps.gga_string)
        #     self.serialDispatcher.send(self.nmeagps.zda_string)
        #     self.serialDispatcher.send(self.nmeagps.rmb_string)
        #     self.serialDispatcher.send(self.nmeagps.rmc_string)

#        print("calculated velocity: ", velocity)

        #self.nmea_digital.set_values(breaker_values)
        self.nmea_digital.createString()
        self.nmea_analog_1.set_values(thrusterload_values)
        self.nmea_analog_1.createString()
        self.nmea_analog_2.set_values(genload_values)
        self.nmea_analog_2.createString()
       # self.nmea_analog_3.set_values([4.3, 4., "##", "##"])
        self.nmea_analog_3.set_values([4.3, 4.1, 0.0, 0.0, 0.0, 0.0])
        self.nmea_analog_3.createString()
        self.nmea_analog_4.set_values([self.gps_data.utc_time,
                                       self.gps_data.utc_date,
                                       'OK', '%.6f'%(self.gps_data.lat()) + 'N',
                                       '%.5f'%(self.gps_data.lon()) + 'E'
                                       , velocity, self.gps_data.heading])
        self.nmea_analog_4.createString()



        if len(self.sent_packets) >= 5:
            self.sent_packets.clear()

        if not 'packet0' in self.sent_packets:
            self.message_dispatcher.send(self.nmea_digital.nmeastring())
            print(self.nmea_digital.nmeastring())
            self.sent_packets.add('packet0')

        if not 'packet1' in self.sent_packets:
            self.message_dispatcher.send(self.nmea_analog_1.nmeastring())
            print(self.nmea_analog_1.nmeastring())
            #logger.debug(self.nmea_analog_1.nmeastring().strip())
            self.sent_packets.add('packet1')

        if not 'packet2' in self.sent_packets:
            self.message_dispatcher.send(self.nmea_analog_2.nmeastring())
            print(self.nmea_analog_2.nmeastring())
#            self.modbusdata.set_flow(112.1)
            #            logger.debug(self.nmea_analog_2.nmeastring().strip())
            self.sent_packets.add('packet2')

        if not 'packet3' in self.sent_packets:
            self.message_dispatcher.send(self.nmea_analog_3.nmeastring())
            print(self.nmea_analog_3.nmeastring())
#            logger.debug(self.nmea_analog_3.nmeastring().strip())
            self.sent_packets.add('packet3')

        if not 'packet4' in self.sent_packets:
            self.message_dispatcher.send(self.nmea_analog_4.nmeastring())
            print(self.nmea_analog_4.nmeastring())
#            logger.debug(self.nmea_analog_4.nmeastring().strip())
            self.sent_packets.add('packet4')

#        if not 'packet5' in sent_packets:
 #           self.message_dispatcher.
