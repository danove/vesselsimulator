#from PyQt5.QtChart import QXYSeries
from PyQt5.QtCore import QObject, pyqtProperty, QUrl, QPoint, pyqtSlot, pyqtSignal
from collections import deque
class GraphData(QObject):
    graphclear = pyqtSignal()
    def __init__(self):
        super(GraphData, self).__init__()
        self.data = [QPoint()] * 500
        self.gui_data = []
        self.x = 0
        for i in range(0, len(self.data)):
            self.data[i] = QPoint(i,0)


    def append(self, value):

        if self.x < len(self.data):

            self.data[self.x].setX(self.x)
            self.data[self.x].setY(value)
            self.gui_data.append(self.data[self.x])
            self.x += 1
        else:
            self.x = 0
            self.graphclear.emit()
            self.gui_data.clear()
            for i in range(0, len(self.data)):
                self.data[i] = QPoint(i, -1)


