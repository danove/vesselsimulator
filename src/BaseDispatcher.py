from abc import ABC, abstractmethod

class BaseDispatcher(ABC):
    def __init__(self, port):
        self.port = port

    @abstractmethod
    def send(self, string):
        
        pass
    
    @abstractmethod
    def disconnect(self):
        pass
