from src import utilsclass
import datetime


class NMEAGps():
    def __init__(self):
        self.gns_string = ''
        self.vtg_string = ''
        self.gns_components = []
        self.vtg_components = []
        self.__chksum = 0

    def createString(self):
        self.createGNS()
        self.createVTG()

    def createGNS(self, gpsdata):
        self.gns_string = ''
        #  HHMMSS.SS
        #  DDMM.MMMMM latitude
        #  N/S        North/South
        #  DDMM.MMMMM longitude
        #  E/W
        #  MM         mode
        #  QQ         num sats used
        #  P.P        HDOP
        #  A.A        antenna altitude
        #  G.G        geoidal separation
        #  SSS        diff signal age
        #  RRRR       differental ref station
        #  NS         navigational status
        #  *CC        crc
        self.gns_components = []
        self.gns_components.append('$GPGNS')
        latitude = utilsclass.convert_dd_to_dms(gpsdata.latitude)
        latitude_DDMM_MM = str(latitude[0]) + str(latitude[1]) + '.' + str(latitude[2]).replace('.', '')
        self.gns_components.append('122312.12')
        self.gns_components.append(latitude_DDMM_MM)
        if gpsdata.latitude < 0:
            self.gns_components.append('S')
        else:
            self.gns_components.append('N')

        longitude = utilsclass.convert_dd_to_dms(gpsdata.longitude)

        longitude_DDMM_MM = str(longitude[0]) + str(longitude[1]) + '.' + str(longitude[2]).replace('.', '')
        self.gns_components.append(longitude_DDMM_MM)

        if gpsdata.longitude < 0:
            self.gns_components.append('W')
        else:
            self.gns_components.append('E')

        #mode
        self.gns_components.append('A')
        #numsatelites
        self.gns_components.append('7')
        #hdop
        self.gns_components.append('1.1')
        #antennaaltitude
        self.gns_components.append('23')
        #geiodal separation
        self.gns_components.append('2')

        self.gns_components.append('ab')
        self.gns_components.append('S')
        self.gns_string = ",".join(self.gns_components)

        self.calcChecksum(self.gns_string)
        self.gns_string = self.appendCRC(self.gns_string)
        self.gns_string = self.appendCRLF(self.gns_string)
        self.gns_string = self.gns_string.encode('utf-8')

    def createVTG(self, gpsdata):
        self.vtg_string = ''
        self.vtg_components = []
        self.vtg_components.append('$GPVTG')
        self.vtg_components.append(str(gpsdata.heading))
        self.vtg_components.append('T')
        self.vtg_components.append(str(gpsdata.heading - 20))
        self.vtg_components.append('M')
        self.vtg_components.append(str(gpsdata.SOG))
        self.vtg_components.append('N')
        self.vtg_components.append(str(gpsdata.SOG * 1.852))
        self.vtg_components.append('K')

        self.vtg_string = ",".join(self.vtg_components)
        self.calcChecksum(self.vtg_string)
        self.vtg_string = self.appendCRC(self.vtg_string)
        self.vtg_string = self.appendCRLF(self.vtg_string)
        self.vtg_string = self.vtg_string.encode('utf-8')

    def createHDT(self, gpsdata):
        pass

    def createGGA(self, gpsdata):
        """
        UTC
        Lat, LatDir
        Lon, LonDir
        Qualitty (0-8)
        NumSats (00 - 0x)
        HDOP (float, 0.x)
        Altitude, M
        Height of GEOID, M
        empty
        empty
        checksum

        """
        timestamp = datetime.datetime.utcnow()
        utctime = timestamp.strftime("%H%M%S")

        self.gga_string = ''
        self.gga_components = []
        self.gga_components.append('$GPGGA')
        self.gga_components.append(utctime)
        self.gga_components.append(str("{:.5f}".format(gpsdata.latitude)))
        self.gga_components.append(gpsdata.latSign)
        self.gga_components.append(str("{:.5f}".format(gpsdata.longitude)))
        self.gga_components.append(gpsdata.longSign)
        self.gga_components.append('8')
        self.gga_components.append('1.2')
        self.gga_components.append('24.1')
        self.gga_components.append('M')
        self.gga_components.append('452.1')
        self.gga_components.append('M')
        self.gga_components.append('')
        self.gga_components.append('')
        self.gga_string = ",".join(self.gga_components)
        self.calcChecksum(self.gga_string)
        self.gga_string = self.appendCRC(self.gga_string)
        self.gga_string = self.appendCRLF(self.gga_string)

        self.gga_string = self.gga_string.encode('utf-8')

    def createRMB(self, gpsdata):
        """
        DataStatus, A=OK, V=Void(Warning)
        0.66, L - Cross-track error, steer L or R to correct
        003, Origin Waypoint ID
        004, Destination Waypoint ID
        4812.24, N - Destination Waypoint Latitude
        12309.57, W - Destination Waypoint Longitude
        001.1  - Range to destination
        052.2 - True Bearing to destination
        000.2 - Velocity towards destination (knots)
        V     - Arrival alarm A = arrived, V not arrived

        """
        timestamp = datetime.datetime.utcnow()
        utctime = timestamp.strftime("%H%M%S")
        self.rmb_string = ''
        self.rmb_components = []
        self.rmb_components.append('$GPRMB')
        self.rmb_components.append('0.21')
        self.rmb_components.append('L')
        self.rmb_components.append('003')
        self.rmb_components.append('005')
        self.rmb_components.append('4917.24')
        self.rmb_components.append('N')
        self.rmb_components.append('12309.57')
        self.rmb_components.append('W')
        self.rmb_components.append('012.1')
        self.rmb_components.append('056.1')
        self.rmb_components.append('004.1')
        self.rmb_components.append('V')
        self.rmb_string = ",".join(self.rmb_components)
        self.calcChecksum(self.rmb_string)
        self.rmb_string = self.appendCRC(self.rmb_string)
        self.rmb_string = self.appendCRLF(self.rmb_string)

        self.rmb_string = self.rmb_string.encode('utf-8')
    def createRMC(self, gpsdata):
        """
        123519 - Fix taken at
        A      - Status. A=Active, V=Void
        4807.038, N - Latitude
        01131.000, E - Longitude
        022.1, SOG in knots
        084.4, Track angle in degrees True
        230119, Date
        003.1, W  - Magnetic variation

        """
        self.rmc_string = ''
        self.rmc_components = []
        self.rmc_components.append('$GPRMC')
        self.rmc_components.append('A')
        self.rmc_components.append('4805.035')
        self.rmc_components.append('N')
        self.rmc_components.append('01131.000')
        self.rmc_components.append('E')
        
        self.rmc_components.append('022.1')
        self.rmc_components.append('005.1')
        self.rmc_components.append('211219')
        self.rmc_components.append('003.1')
        self.rmc_components.append('W')
        self.rmc_string = ",".join(self.rmb_components)
        self.calcChecksum(self.rmc_string)
        self.rmc_string = self.appendCRC(self.rmc_string)
        self.rmc_string = self.appendCRLF(self.rmc_string)
        self.rmc_string = self.rmc_string.encode('utf-8')

    def createZDA(self, gpsdata):
        timestamp = datetime.datetime.utcnow()
        utctime = timestamp.strftime("%H%M%S")
        self.zda_string = ''
        self.zda_components = []
        self.zda_components.append('$GPZDA')
        self.zda_components.append(utctime)
        self.zda_components.append('04')
        self.zda_components.append('12')
        self.zda_components.append('2019')
        self.zda_components.append('00')
        self.zda_components.append('00')

        self.zda_string = ",".join(self.zda_components)
        self.calcChecksum(self.zda_string)
        self.zda_string = self.appendCRC(self.zda_string)
        self.zda_string = self.appendCRLF(self.zda_string)
        self.zda_string = self.zda_string.encode('utf-8')

    def calcChecksum(self, string):
        i = 1
        while i < len(string):
            char = ord(string[i])
            self.__chksum = self.__chksum ^ char
            i = i + 1

    def appendCRC(self, nmeastring):
        nmeastring = nmeastring + '*' + hex(self.__chksum)[2:]
        return nmeastring

    def appendCRLF(self, nmeastring):
        nmeastring = nmeastring + '\r\n'
        return nmeastring

    
    
    
