#!/usr/bin/python
import sys
from src.nmeacreator import NMEACreator
class NMEASwbdStates(NMEACreator):

    def __init__(self):
        self.__statusbits = [0] * 21
        self.__type = 'S'
        NMEACreator.__init__(self, self.__type)

    def createString(self):
        NMEACreator.createString(self)
        nmeastring = NMEACreator.nmeastring(self)
        print(nmeastring)
        for value in self.__statusbits:
            nmeastring = nmeastring + ',' + str(value)

        self.set_nmeastring(nmeastring)
        self.calcChecksum()
        self.appendCRC()
        self.appendCRLF()

    def set_values(self, values):
        self.__statusbits = values



    
