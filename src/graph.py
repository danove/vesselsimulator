import os
import sys
import proto.configuration_pb2 as configuration
from src.GraphNode import GraphNode
from src import generator, thruster, breaker, bus, bustie
class Graph():


    def __init__(self, proto):
        self.__proto = proto
        self.adj_list = {}
        self.__thrusterproto = self.__proto.thruster

        self.__generators = [] # self.__switchboard.generators
        self.__generatorbreakers = [] # self.__switchboard.generatorbreakers
        self.__thrusterbreakers  = [] #self.__switchboard.thrusterbreakers
        self.__propulsors = [] #self.__switchboard.propulsors
        self.__buses = [] #self.__switchboard.buses
        self.__busties = [] #self.__switchboard.busties
        self.generatorlist = []
        self.thrusterlist = []


    def createAdjacenciesList(self):
        for bus in self.__buses:
            self.adj_list[bus] = []

        idx = 0
        for generator in self.__generators:
            adjacentbreakerindex = self.__proto.generator[idx].generatorbreakerindex
            self.adj_list[generator] = [self.__generatorbreakers[adjacentbreakerindex - 1]]
            idx = idx + 1
        idx = 0
        for propulsor in self.__propulsors:
            adjacentbreakerindex = self.__proto.thruster[idx].thrusterbreakerindex
            self.adj_list[propulsor] = [self.__thrusterbreakers[adjacentbreakerindex - 1]]
            idx = idx + 1
        idx = 0
        for generatorbreaker in self.__generatorbreakers:
            adjacentgeneratorindex = self.__proto.generatorbreaker[idx].generatorindex
            adjacentbusindex = self.__proto.generatorbreaker[idx].busIndex
            self.adj_list[generatorbreaker] = [self.__buses[adjacentbusindex - 1],
                                               self.__generators[adjacentgeneratorindex - 1]]

            self.adj_list[self.__buses[adjacentbusindex-1] ].append(generatorbreaker)
            idx = idx + 1
        idx = 0
        for thusterbreaker in self.__thrusterbreakers:
            adjacentthrusterindex = self.__proto.thrusterbreaker[idx].thrusterIndex
            adjacentbusindex = self.__proto.thrusterbreaker[idx].busIndex
            self.adj_list[thusterbreaker] = [self.__buses[adjacentbusindex - 1],
                                             self.__propulsors[adjacentthrusterindex - 1]]
            self.adj_list[self.__buses[adjacentbusindex-1]].append(thusterbreaker)
            idx = idx + 1

        idx = 0
        for bustie in self.__busties:
            adjacentbus1index = self.__proto.bustie[idx].bus1Index
            adjacentbus2index = self.__proto.bustie[idx].bus2Index
            self.adj_list[bustie] = [self.__buses[adjacentbus1index - 1], self.__buses[adjacentbus2index - 1]]
        #    self.adj_list[self.__buses[adjacentbus1index]].append(bustie)

    def addEdge(self, v, w):
        pass

    def deleteEdge(self, v, w):
        pass


    def setGenerators(self, gens):
        self.__generators = gens

    def setGeneratorBreakers(self, genbreakers):
        self.__generatorbreakers = genbreakers

    def setThrusterBreakers(self, thrbreakers):
        self.__thrusterbreakers = thrbreakers

    def setPropulsors(self, props):
        self.__propulsors = props

    def setBuses(self, buses):
        self.__buses = buses

    def setBusTies(self, busties):
        self.__busties = busties


    def findThrustersConnectedToNode(self, node):
        if node not in self.adj_list:
            return None
        elif isinstance(node, thruster.Thruster):
            self.thrusterlist.append(node)
        else:
            for element in self.adj_list[node]:

                if not element.visited():
                    if isinstance(element, thruster.Thruster):
                        self.thrusterlist.append(element)
                        element.setVisited(True)
                    else:
                        element.setVisited(True)
                        self.findThrustersConnectedToNode(element)
        return self.thrusterlist

    def findGeneratorsConnectedToNode(self, node):
        if node not in self.adj_list:
            return None
        elif isinstance(node, generator.Generator):
            self.generatorlist.append(node)
        else:
            for element in self.adj_list[node]:

                if not element.visited():
                    if isinstance(element, generator.Generator):
                        self.generatorlist.append(element)
                        element.setVisited(True)
                    else:
                        element.setVisited(True)
                        self.findGeneratorsConnectedToNode(element)
        return self.generatorlist
