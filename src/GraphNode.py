import sys

from PyQt5.QtCore import QObject
class GraphNode(QObject):

    def __init__(self):
        super(GraphNode, self).__init__()
        self._visited = False

    def setVisited(self, visited):
        self._visited = visited

    def visited(self):
        return self._visited
