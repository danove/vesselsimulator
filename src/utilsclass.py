
def convert_dd_to_ddm(dd):
    decimal_part = dd % 1
    degrees = int(dd-decimal_part)
    decimal_minutes = decimal_part * 60
    decimal_minutes = round(decimal_minutes, 4)
    return (degrees, decimal_minutes)


def convert_dd_to_dms(dd):
    degrees = int(dd)
    dec_part = dd % 1
    
    dm = dec_part * 60
    minutes = int(dm)
    seconds = (dm % 1) * 60
    seconds = round(seconds, 3)
    return (degrees, minutes, seconds)


def convert_percent_to_rawvalue(percentValue,
                                min=745,
                                max=3723):
    deltaVal = (max-min) * (percentValue / 100.0)
    rawVal = min + deltaVal
    return rawVal

