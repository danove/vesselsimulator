import sys
import logging
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from apscheduler.schedulers import blocking, qt

from PyQt5.QtWidgets import QApplication
from PyQt5.QtQuick import QQuickView
from PyQt5.QtCore import QUrl
from src import switchboard, ObjectFactory, graph, networkserver, messagedispatcher, dispatcher
import proto.configuration_pb2 as configuration
import google.protobuf.text_format as tf
import proto.switchboard_pb2 as switchboardproto

# Set up suitable application-wide logger
logger = logging.getLogger("vesselsimulator")

__log_handler = logging.StreamHandler()
__log_handler.setLevel(logging.DEBUG)

__log_formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
__log_handler.setFormatter(__log_formatter)

logger.addHandler(__log_handler)


class Simulator():

    def __init__(self, swb, dispatcher):
        self.switchboard = swb
        self.dispatcher = dispatcher
        scheduler = qt.QtScheduler()

        scheduler.add_job(self.switchboard.time_step, 'interval', seconds=0.2)
        scheduler.add_job(self.dispatcher.time_step, 'interval', seconds=0.2, max_instances=3)
        scheduler.start()


if __name__ == '__main__':
    # Define and parse arguments
    argparser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        description=
            "Use -v multiple times to increase verbosity, "
            "twice will print all outgoing packets"
    )
    argparser.add_argument("-v", "--verbose", action="count", help="Increase verbosity")
    argparser.add_argument("--target-ip", default="192.168.1.2",
                           help="Target IP for AIS UDP packets")
    argparser.add_argument("--target-port", type=int, default=10000,
                           help="Target port for AIS UDP packets")

    args = argparser.parse_args()

    if args.verbose:
        logger.setLevel(logging.INFO)

        # If verbosity specified more than once, increase verbosity to maximum level
        if args.verbose > 1:
            logger.setLevel(logging.DEBUG)

    app = QApplication(sys.argv)
    config_file = 'ohmeling_conf.txt'
    config_proto = configuration.Configuration()
    object_factory = ObjectFactory.ObjectFactory(config_file)

    configFile = open(config_file, 'r')

    messageDispatcher = messagedispatcher.MessageDispatcher(args.target_ip,
                                                            args.target_port)
    tf.Merge(configFile.read(), config_proto)
    switchboard_proto = switchboardproto.SwitchBoard()
    graph = graph.Graph(config_proto)
    switchboard = switchboard.Switchboard(object_factory, graph, switchboard_proto)
    dispatcher = dispatcher.Dispatcher(switchboard_proto, messageDispatcher)

    simulator = Simulator(switchboard, dispatcher)
    view = QQuickView()
#    view.setSource(QUrl.fromLocalFile('qml/simulator.qml'))
    view.setSource(QUrl.fromLocalFile('qml/main.qml'))
    context = view.rootContext()
    context.setContextProperty('switchboard', switchboard)

 

    view.show()


    sys.exit(app.exec_())
