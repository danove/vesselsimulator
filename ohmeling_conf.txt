
generator {
index: 1
generatorbreakerindex: 1
name: "generator 1"
maxPower : 2200.0
}	
generator {
index:2
generatorbreakerindex: 2
name: "gen2"
maxPower : 2200.0

}
generator {
index: 3
generatorbreakerindex: 3
name: "gen3"
maxPower : 2200.0
}
generator {
index: 4
generatorbreakerindex: 4
name: "gen4"
maxPower : 2200.0
}

generatorbreaker {
index: 1
generatorindex: 1
name: "gen breaker 1"
busIndex : 1
}

generatorbreaker {
index: 2
generatorindex: 2
name: "gen breaker 2"
busIndex : 1
}
generatorbreaker {
index: 3
generatorindex: 3
name: "gen breaker 3"
busIndex : 2
}
generatorbreaker {
index: 4
generatorindex: 4
name: "gen breaker 4"
busIndex : 2
}

bus {
index: 1

name: "Port Bus"


}

bus {
index: 2

name: "Port Bus"
}

bustie{
index: 1
bus1Index: 1
bus2Index: 2
name: "bustie"
}

thrusterbreaker {
index: 1
name: "thrusterbreaker 1"
thrusterIndex: 1
busIndex: 1

}

thrusterbreaker {
index: 2
name: "thrusterbreaker 2"
thrusterIndex: 3
busIndex: 1

}
thrusterbreaker {
index: 3
name: "thrusterbreaker 3"
thrusterIndex: 4
busIndex: 1

}
thrusterbreaker {
index: 4
name: "thrusterbreaker 4"
thrusterIndex: 2
busIndex: 2

}
thrusterbreaker {
index: 5
name: "thrusterbreaker 5"
thrusterIndex: 5
busIndex: 2

}


thruster {
	 index: 1
	 name: "BowTunnel1"
	 maxPower : 880.0
	 thrusterbreakerindex:1

}

thruster {
	 index: 2
	 name: "BowTunnel2"
	 maxPower : 880.0
	 thrusterbreakerindex:4
}
thruster {
	 index: 3
	 name: "BowAzimuth"
	 maxPower : 1200.0
	 thrusterbreakerindex:2

}
thruster {
	 index: 4
	 name: "PortAzipull"
	 maxPower : 3500.0
	 thrusterbreakerindex:3
}
thruster {
	 index: 5
	 name: "StbdAzipull"
 	 maxPower : 3500.0
	 thrusterbreakerindex:5
}
