import unittest

from context import src
from src import utilsclass

class UtilsTestCase(unittest.TestCase):

    def test_convert_from_dd_to_ddm(self):
        decimal_degrees = 59.171657
        expected_result = (59, 10.2994)
        result = utilsclass.convert_dd_to_ddm(decimal_degrees)
        self.assertTupleEqual(expected_result, result)

    def test_convert_from_dd_to_dms(self):
        decimal_degrees = 59.171657
        expected_result = (59, 10, 17.965)
        self.assertTupleEqual(expected_result,
                              utilsclass.convert_dd_to_dms(decimal_degrees))

    def test_convert_percent_to_rawvalue(self):
        percentVal = 50
        expected_result = 2234.0
        self.assertEqual(expected_result, utilsclass.convert_percent_to_rawvalue(percentVal))

    def test_convert_percent_to_rawvalue2(self):
        percentVal = 100
        expected_result = 3723.0
        self.assertEqual(expected_result, utilsclass.convert_percent_to_rawvalue(percentVal))

    def test_convert_percent_to_rawvalue3(self):
        percentVal = 0
        expected_result = 745.0
        self.assertEqual(expected_result, utilsclass.convert_percent_to_rawvalue(percentVal))


if __name__ == '__main__':
    unittest.main()
