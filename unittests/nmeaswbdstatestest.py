import unittest

from context import src
from src import nmeaswbdstates
class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.nmeaswbdstates = nmeaswbdstates.NMEASwbdStates()

    def test_create_base_string_shallreturnPWNOS(self):
        expected_string = '$PWNOS,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0'

        self.nmeaswbdstates.createString()

        self.assertEqual(expected_string, self.nmeaswbdstates.nmeastring())

    def test_create_string_with_crc(self):
        expected_string = '$PWNOS,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*49'
        self.nmeaswbdstates.createString()
        self.nmeaswbdstates.calcChecksum()
        self.nmeaswbdstates.appendCRC()
        self.assertEqual(expected_string, self.nmeaswbdstates.nmeastring())


    def test_create_complete_string_with_crc_and_crlf(self):
        expected_string = '$PWNOS,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*49\r\n'
        self.nmeaswbdstates.createString()
        self.nmeaswbdstates.calcChecksum()
        self.nmeaswbdstates.appendCRC()
        self.nmeaswbdstates.appendCRLF()
        self.assertEqual(expected_string, self.nmeaswbdstates.nmeastring())

if __name__ == '__main__':
    unittest.main()

    
