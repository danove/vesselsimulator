import unittest

from context import src
from src import nmeagps
from src import gpscoordinates


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.nmeagps = nmeagps.NMEAGps()
        self.gpsdata = gpscoordinates.GpsData()
        
    def test_creation_of_gns_string(self):
        self.gpsdata.setLatitude(59.171657)
        self.nmeagps.createGNS(self.gpsdata)
        print(self.nmeagps.gns_string)

    def test_creation_of_vtg_string(self):
        self.gpsdata.setLatitude(59.171657)
        self.gpsdata.setSpeedOverGround(7.8)
        self.gpsdata.setVesselHeading(145.3)
        self.nmeagps.createVTG(self.gpsdata)
        print(self.nmeagps.vtg_string)
        
if __name__ == '__main__':
    unittest.main()
