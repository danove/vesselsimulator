import unittest

from context import src
from src import nmeaanalogvalues
class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.nmeanalogvalues_1 = nmeaanalogvalues.NMEAAnalogValues(1, 5)
        self.nmeanalogvalues_2 = nmeaanalogvalues.NMEAAnalogValues(2, 4)
        self.nmeanalogvalues_3 = nmeaanalogvalues.NMEAAnalogValues(3, 6)
        self.nmeanalogvalues_4 = nmeaanalogvalues.NMEAAnalogValues(4, 7)

    def test_correctbasestringshallbecreated_basedontelegramindex(self):
        expected_string = '$PWNOM1,0.0,0.0,0.0,0.0,0.0'
        self.nmeanalogvalues_1.createString()
        self.assertEqual(expected_string, self.nmeanalogvalues_1.nmeastring())


    def test_telegramtype3_shall_be_created_correctly(self):
        expected_string = '$PWNOM3,0.0,0.0,0.0,0.0,0.0,0.0'
        
        self.nmeanalogvalues_3.createString()
        self.assertEqual(expected_string, self.nmeanalogvalues_3.nmeastring())



    def test_telegramtype3_shall_be_created_correctly_withCRC_andCRLF(self):
        expected_string = '$PWNOM3,0.0,0.0,0.0,0.0,0.0,0.0*78\r\n'
        self.nmeanalogvalues_3.createString()
        self.nmeanalogvalues_3.calcChecksum()
        self.nmeanalogvalues_3.appendCRC()
        self.nmeanalogvalues_3.appendCRLF()
        self.assertEqual(expected_string, self.nmeanalogvalues_3.nmeastring())
        
        
        


if __name__ == '__main__':
    unittest.main()

    
