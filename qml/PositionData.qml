import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2

Rectangle {
    id: positionData
    width: 200
    height: 200
    color: "yellow"

    GridLayout {
	flow: GridLayout.TopToBottom
	rows: 4
	TextField {
	    placeholderText: "LatPos"
	}
	TextField {
	    placeholderText: "LonPos"
	}
	TextField {
	    placeholderText: "SOG"
	}
	TextField {
	    placeholderText: "hdg"   
	}
	Text {
	    text: "Lat"
	}
	Text {
	    text: "Lon"
	}
	Text {
	    text: "Speed Over Ground"
	}
	Text {
	    text: "Heading"
	}


    }
}
