import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2

Rectangle {
    id: generatorData


    GridLayout{
	id: jau
	rows: switchboard.get_number_of_generators() + 1
	Layout.maximumWidth: 40
	Layout.maximumHeight: 20
	flow: GridLayout.TopToBottom



	Text {text: "Generator Name"}
	Repeater {
	    model: switchboard.get_number_of_generators()

	    Text{
		color: "black"
		text: switchboard.generatorName(index)
	    }
	
	}
	Text {text: "Load In kW"} 
	Repeater {
	    model: switchboard.get_number_of_generators()
	    TextField{
		Layout.maximumWidth: 80
    	    	placeholderText: "load"
		onEditingFinished: {
		    switchboard.setGeneratorLoad(index, parseInt(displayText))
		}
    	    }
    	}
	Text {text: "Flow to engine l/h"}
	Repeater {
	    model: switchboard.get_number_of_generators()
	    TextField{
		Layout.maximumWidth: 80
    	    	placeholderText: "flow"
		onEditingFinished: {
		    switchboard.setDieselFlowToEngine(index, parseInt(displayText))
		    console.log("index: ", index)
		}
    	    }
	}

	Text {text: "Flow from engine l/h"}
	Repeater {
	    model: switchboard.get_number_of_generators()
	    TextField{
		Layout.maximumWidth: 80
    	    	placeholderText: "flow"
		onEditingFinished: {
		    switchboard.setDieselFlowFromEngine(index, parseInt(displayText))
		}
    	    }
	}
	Text {text: "Urea Flow l/h"}
	Repeater {
	    model: switchboard.get_number_of_generators()
	    TextField{
		Layout.maximumWidth: 80
    	    	placeholderText: "urea"
		onEditingFinished: {
		    switchboard.setUreaFlow(index, parseInt(displayText))
		}
    	    }
	}
	

	
    }

}
