import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2

Rectangle {
    id: propulsorData

    GridLayout{
	id: jau
	rows: switchboard.get_number_of_propulsors() + 1
	Layout.maximumWidth: 40
	Layout.maximumHeight: 20
	flow: GridLayout.TopToBottom



	Text {text: "Thruster Name"}
	Repeater {
	    model: switchboard.get_number_of_propulsors()

	    Text{
		color: "black"
		text: switchboard.propulsorName(index)
	    }
	    
	}
	Text {text: "Load In kW"} 
	Repeater {
	    model: switchboard.get_number_of_propulsors()
	    TextField{
		Layout.maximumWidth: 80

    	    	placeholderText: "load"
		onEditingFinished: {
		    console.log(".........................Jau..................")
		    switchboard.setThrustOrderInKW(index, parseInt(displayText))
		}
    	    }
    	}
	
    }

    Connections{
	target: switchboard
    }
}
