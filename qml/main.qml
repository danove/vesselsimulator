import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2

ApplicationWindow {
    id: simulatorRoot
    width: 800
    height: 600
    visible:true
    color: "#16161b"

    GridLayout {
	id: grid
	columns: 2
	GeneratorData {
	    Layout.preferredWidth: 650
	    Layout.preferredHeight: 200
	}
	PropulsorData {
	    Layout.preferredWidth: 350
	    Layout.preferredHeight: 300
	    
	}

	PositionData {
	    Layout.preferredWidth: 350
	    Layout.preferredHeight: 200
	} 


    }
    Connections {
	target: switchboard
    }
}
