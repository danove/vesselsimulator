import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2

Rectangle {
    id: generatorView
    width: 100
    height: 100
    
    CircularGauge {
	property string gaugetext: ''
		property int maxValue: 100
	property int minValue: 0
	property double inputValue: 0
        value: inputValue
        maximumValue: maxValue
        minimumValue: 0
        stepSize: 10
        opacity: 80
        style: CircularGaugeStyle {
                    id: style
                    tickmarkStepSize: 200
                    function degreesToRadians(degrees) {
                        return degrees * (Math.PI / 180);
                    }

                    background: Canvas {
                        onPaint: {
                            var ctx = getContext("2d");
                            ctx.reset();
                            ctx.beginPath();
                            ctx.strokeStyle = "green"
                            ctx.lineWidth = outerRadius * 0.04;
                            ctx.arc(outerRadius,
                                    outerRadius,
                                    outerRadius - ctx.lineWidth / 2,
                                    degreesToRadians(valueToAngle(0.42*maxValue)),
                                    degreesToRadians(valueToAngle(0.6*maxValue)), false);
                            ctx.stroke();
                        }
                    }

                    tickmark: Rectangle {
                        visible: styleData.value <= 0.75*gauge.maximumValue && styleData.value >= 0.85*gauge.maximumValue ? false : true

                        implicitWidth: outerRadius * 0.02
                        antialiasing: true
                        implicitHeight: outerRadius * 0.06
                        color: styleData.value >= 0.85*gauge.maximumValue && styleData.value <= 0.75*gauge.maximumValue ? "green" : "white"
                    }

                    minorTickmark: Rectangle {
                        visible: styleData.value < 0.75*gauge.maximumValue || styleData.value > 0.85*gauge.maximumValue
                        implicitWidth: outerRadius * 0.01
                        antialiasing: true
                        implicitHeight: outerRadius * 0.03
                        color: "white"
                    }

                    tickmarkLabel:  Text {
                        font.pixelSize: Math.max(6, outerRadius * 0.1)
                        text: styleData.value
                        color: styleData.value <= 0.85*gauge.maximumValue && styleData.value >= 0.75*gauge.maximumValue ? "green" : "white"
                        //color: styleData.value >= 85 ? "#e34c22" : "#e5e5e5"
                        antialiasing: true
                    }

                    needle: Rectangle {
                        y: outerRadius * 0.15
                        implicitWidth: outerRadius * 0.03
                        implicitHeight: outerRadius * 0.9
                        antialiasing: true
                        color: "#e5e5e5"
                    }

                    foreground: Item {
                        Rectangle {
                            width: outerRadius * 0.2
                            height: width
                            radius: width / 2
                            color: "#e5e5e5"
                            anchors.centerIn: parent
                        }
                    }
                }
        Text {
            id: kwText
            text: 'kW'
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: indexText.top
            color: "white"
        }
        Text {
            id: indexText
            text: gaugetext
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            color: "#BFBFBF"
            font.family: "Arial"
            font.pixelSize: 12
          }
    }
}
