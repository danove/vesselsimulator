import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2
import QtQuick 2.0
Rectangle {
    id:mainWindow
    width: 1200
    height: 1200

    Rectangle {
	id: generatorView
	width: 800
	height: 200

	RowLayout {
	    id: generatorsLayout
	    GeneratorAndBreaker{}
	    GeneratorAndBreaker{}
	    GeneratorAndBreaker{}
	    GeneratorAndBreaker{}
	}
    }

    Rectangle {
	id: busView
	width: 1000
	height: 1
	anchors.top: generatorView.bottom

	Path {
	    id: portBusPath
	    startX: 0; startY: 0
	    PathLine {x:400; y:0}
	}
	
	Path {
	    id: stbdBusPath
	    startX: 0; startY: 0
	    PathLine {x:400; y:0}

	}
	RowLayout {
	    id: busLayout
	    anchors.fill: parent
	    spacing: 1
	    Canvas {
	    	id: portBusCanvas
		// canvas size
		width: 400; height: 60
		// handler to override for drawing
		onPaint: {
		    // get context to draw with
		    var ctx = getContext("2d")
		    ctx.lineWidth = 2
		    ctx.path = portBusPath
		    ctx.stroke()
		}
	    }
	    Image {
		source: "horizontal_open_switch.png"
		
	    }
	    Canvas {
	    	id: stbdbusCanvas
		// canvas size
		width: 850; height: 60
		// handler to override for drawing
		onPaint: {
		    // get context to draw with
		    var ctx = getContext("2d")
		    ctx.lineWidth = 2
		    ctx.path = stbdBusPath
		    ctx.stroke()

		}
	    }
	    
	}



	
    }
    Rectangle {
	id: thrusterView
	width: 800
	height: 200
	//color: "blue"
	anchors.top: busView.bottom
	RowLayout {
	    id: thrustersLayout

	    ThrusterAndLever {}
	    ThrusterAndLever {}
	    ThrusterAndLever {}
	    ThrusterAndLever {}
	    ThrusterAndLever {}
	    
	}
    }
    
}
