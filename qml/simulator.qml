import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2
Rectangle {
    id: posView
    width: 600
    height: 300
    color: 'grey'

    radius: 9
    border.width: 3
    border.color: "#16161b"
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#000000"
        }

        GradientStop {
            position: 0.5
            color: "#181618"
        }

        GradientStop {
            position: 1
            color: "#0D0C0D"
        }

    }

    RowLayout {
	id: sliderLayout
	Slider {
            id:orderBT1
            from: 0.0
            to: 100.0
            stepSize: 1.0

            orientation: Qt.Vertical
            onMoved: {
		switchboard.setThrustOrderInPct(1, value)

            }
	}

	Slider {
            id:orderLeverBT2
            from: 0.0
            to: 100.0
            stepSize: 1.0

            orientation: Qt.Vertical
            onMoved: {
		switchboard.setThrustOrderInPct(2, value)

            }
	}
	Slider {
            id:orderLeverBAzi
            from: 0.0
            to: 100.0
            stepSize: 1.0

            orientation: Qt.Vertical
            onMoved: {
		switchboard.setThrustOrderInPct(3, value)

            }
	}

	Slider {
            id:orderLeverStbd
            from: 0.0
            to: 100.0
            stepSize: 1.0

            orientation: Qt.Vertical
            onMoved: {
		switchboard.setThrustOrderInPct(5, value)
		//stbdPropulsion.set_order_pct(value)
            }
	}
	Slider {
            id:orderLeverPort
            from: 0.0
            to: 100.0
            stepSize: 1.0

            orientation: Qt.Vertical
            onMoved: {
		switchboard.setThrustOrderInPct(4, value)
		//portPropulsion.set_order_pct(value)
            }
	}
    }

    GridLayout {
	id: environmentalInput
	anchors.left: sliderLayout.right
	columns: 1
	rows: 7

	TextField {
	    placeholderText:qsTr("WaveHs")
	    color: "black"
	}


	TextField {
	    placeholderText:qsTr("Wave Dir")
	    color: "black"
	}


	TextField {
	    placeholderText:qsTr("Current Velocity")
	    color: "black"
	}

	TextField {
	    placeholderText:qsTr("Current Dir")
	    color: "black"
	}


	TextField {
	    placeholderText:qsTr("Wind Velocity")
	    color: "black"
	}

	TextField {
	    placeholderText:qsTr("Wind Dir")
	    color: "black"
	}

	ComboBox {
	    model: ["DP", "Transit", "QuaySide"]
	}
    }




    GridLayout {
	id: inputGrid
	anchors.left: environmentalInput.right
	rows: 7
	columns: 1
	Text {
	    text:qsTr("Latitude")
	    color: "white"
	}
	Text {
	    text:qsTr("Longitude")
	    color: "white"
	}


	Text {
	    text:qsTr("Heading")
	    color: "white"
	}

	Text {
	    text:qsTr("SOG")
	    color: "white"
	}
	Text {
	    text:qsTr("fuel l/h")
	    color: "white"
	}

	Text {
	    text:qsTr("fuel rate")
	    color: "white"
	}
	
    }
}
