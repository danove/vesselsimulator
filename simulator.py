import sys
from apscheduler.schedulers import qt

from PyQt5.QtWidgets import QApplication
# from PyQt5.QtQuick import QQuickView
# from PyQt5.QtCore import QUrl
from pymodbus.server.sync import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import (
    ModbusSlaveContext, ModbusServerContext, ModbusSequentialDataBlock
)
from src import (
    ObjectFactory, graph,
    messagedispatcher, modbusdata)
from threading import Thread
from src.dispatcher import Dispatcher
from src.switchboard import Switchboard
import app.proto.configuration_pb2 as configuration
import google.protobuf.text_format as tf
# import proto.switchboard_pb2 as switchboardproto
import src.gpscoordinates as gps
import src.scenario as scenario
import serial
import serial.rs485
from src.SerialDispatcher import SerialDispatcher
from PyQt5.QtQml import QQmlApplicationEngine
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from src.nmeagps import NMEAGps
from src.serialgpsdispatcher import SerialGpsDispatcher
from subprocess import check_output
import logging
from src.iasdata import IASData
logger = logging.getLogger("simulator")
__log_handler = logging.StreamHandler()
__log_handler.setLevel(logging.INFO)
logger.addHandler(__log_handler)


class Simulator():

    def __init__(self, swb, dispatcher):
        self.logger = logging.getLogger("simulator.Simulator")
        self.logger.info("Simulator object created")
        self.switchboard = swb
        self.dispatcher = dispatcher
        self.scenario = scenario.Scenario(self.switchboard)
        scheduler = qt.QtScheduler()

        scheduler.add_job(self.switchboard.time_step, 'interval', seconds=1)
        scheduler.add_job(self.dispatcher.time_step,
                          'interval', seconds=1, max_instances=3)

        scheduler.start()


def main(args=None):
    logger = logging.getLogger("simulator")
    logger.setLevel(logging.INFO)

    if args is None:
        args = sys.argv[1:]
        argparser = ArgumentParser(
            formatter_class=ArgumentDefaultsHelpFormatter,
            description="Use -c to indicate config file")

        argparser.add_argument("-c", "--config", help="Ecometer config file")

        args = argparser.parse_args()

    app = QApplication(sys.argv)
    config_file = args.config

    config_proto = configuration.Configuration()
    object_factory = ObjectFactory.ObjectFactory(config_file)
    identity = ModbusDeviceIdentification()
    identity.VendorName = "LEVEL Power & Automation"
    identity.ProductName = "vesselsim server"
    identity.ProductCode = "LETS"

    configFile = open(config_file, 'r')
    tf.Merge(configFile.read(), config_proto)

    if config_proto.HasField("iasconnection"):
        logger.info("IAS connection to be set up")
        hostname = config_proto.iasconnection.udp.hostname
        
        port = int(config_proto.iasconnection.udp.portnumber)
        messageDispatcher = messagedispatcher.MessageDispatcher(hostname, port)
    else:
        messageDispatcher = None

    if config_proto.HasField("modbusconnection"):
        logger.info("Modbus connection to be set up")
        modbus_hostname = check_output(['hostname', '--all-ip-addresses'])
        modbus_hostname = modbus_hostname.decode('utf-8')
        modbus_hostname = modbus_hostname.rstrip(' ')
        modbus_hostname = modbus_hostname.rstrip('\n')
        modbus_port = 5020

        modbus_hostname = "172.24.0.11"

        print(modbus_hostname)
        modbus_address = (modbus_hostname, modbus_port)
        print(modbus_address)
        slave_context = ModbusSlaveContext(
            r=ModbusSequentialDataBlock(0, 0),
            zero_mode=True,
        )
        server_context = ModbusServerContext(slaves=slave_context, single=True)
        t = Thread(target=StartTcpServer,
                   args=(server_context, identity, modbus_address),
                   daemon=True)
        t.start()
        modbus_data = modbusdata.ModbusData(server_context[0], config_proto)
    else:
        modbus_data = None
        logger.info("Modbus not found")
    if config_proto.gpsdata.interface.HasField("serialinterface"):
        logger.info("GPS Serial interface found")
        comport = config_proto.gpsdata.interface.serialinterface.port
        baudrate = config_proto.gpsdata.interface.serialinterface.baudrate
        comport = '/dev/ttyUSB0'
        print(comport, baudrate)
        serialport = serial.rs485.RS485(comport, baudrate)
#        serialport = serial.Serial(comport, baudrate)

        settings_485 = serial.rs485.RS485Settings()
        settings_485.rts_level_for_tx = True
        settings_485.rts_level_for_rx = True
        serialport.rs485_mode = settings_485
        serialDispatcher = SerialDispatcher(serialport)
        serialgpsdispatcher = SerialGpsDispatcher(serialport)
    else:
        logger.info("serial gps not found")
        serialport = None
        serialDispatcher = None

    the_graph = graph.Graph(config_proto)
    gpsdata = gps.GpsData()
    nmeagps = NMEAGps()
    iasdata = IASData(config_proto)
    serialgpsdispatcher = None
    switchboard = Switchboard(object_factory,
                              the_graph, nmeagps,
                              config_proto, modbus_data,
                              gpsdata,
                              serialgpsdispatcher,
                              iasdata)

    dispatcher = Dispatcher(messageDispatcher,
                            modbus_data, switchboard.generators,
                            gpsdata, serialDispatcher,
                            iasdata)
    Simulator(switchboard, dispatcher)

    engine = QQmlApplicationEngine()
    context = engine.rootContext()

    context.setContextProperty('switchboard', switchboard)
    engine.load('qml/main.qml')
    win = engine.rootObjects()[0]

    win.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

#     app = QApplication(sys.argv)

#     config_file = 'arriva_pretest_sim.txt'
#     config_proto = configuration.Configuration()
#     print(config_proto)
#     object_factory = ObjectFactory.ObjectFactory(config_file)
    
#     identity = ModbusDeviceIdentification()
#     identity.VendorName = "LEVEL Power & Automation"
#     identity.ProductName = "vesselsim server"
#     identity.ProductCode = "LPSTS"
# #    address = ('172.24.0.11', 5020)
#     address = ('localhost', 5020)p
#     configFile = open(config_file, 'r')

#     messageDispatcher = messagedispatcher.MessageDispatcher('localhost', 10000)
#     serialport = serial.Serial('/dev/ttyS1', 4800)

#     serialDispatcher = SerialDispatcher(serialport)
    
#     tf.Merge(configFile.read(), config_proto)
#     switchboard_proto = switchboardproto.SwitchBoard()



# graph = graph.Graph(config_proto)
    # slave_context = ModbusSlaveContext(
    #     r=ModbusSequentialDataBlock(0, 0),
    #     zero_mode=True,
    # )

    # server_context = ModbusServerContext(slaves=slave_context, single=True)
    # modbusdata = modbusdata.ModbusData(server_context[0], config_proto)
    # gpsdata = gpsdata.GpsData()
    # switchboard = switchboard.Switchboard(object_factory,
    #                                       graph, switchboard_proto,
    #                                       config_proto, modbusdata,
    #                                       gpsdata)

    # dispatcher = dispatcher.Dispatcher(messageDispatcher,
    #                                    modbusdata, switchboard.generators,
    #                                    gpsdata, serialDispatcher)

    # simulator = Simulator(switchboard, dispatcher)

    # engine = QQmlApplicationEngine()
    # context = engine.rootContext()

    # context.setContextProperty('switchboard', switchboard)
    # engine.load('qml/main.qml')
    # win = engine.rootObjects()[0]
    # print("address : ", address)
    # t = Thread(target=StartTcpServer,
    #            args=(server_context, identity, address),
    #            daemon=True)
    # t.start()
    # win.show()
    # sys.exit(app.exec_())
